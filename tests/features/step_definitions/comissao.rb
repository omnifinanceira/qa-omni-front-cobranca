@acordo = AcordoAgentePage.new
@classe_login = LoginPage.new
$comissao = ComissaoPage.new

Então('devo preencher o campo contrato {string} e clicar no botão {string}') do |contrato, pesquisar|
    @acordo.pesquisa_contrato(contrato, pesquisar)
end

Então('validar o contrato de veículos leves') do
    within_frame(find(:xpath, "//iframe")) do 
        expect($comissao).to have_contrato_veiculos_leves
    end
end

Então('validar o contrato de veículos pesados') do
    within_frame(find(:xpath, "//iframe")) do 
        expect($comissao).to have_contrato_veiculos_pesados
    end
end

Então('validar o contrato de comissao de giro') do
    within_frame(find(:xpath, "//iframe")) do 
        expect($comissao).to have_contrato_capital_giro
    end
end

Quando('o botão {string} for clicado') do |boleto|
    $comissao.consulta_divida(boleto)
end

Então('verificar se a janela de confirmação será exibida e vou clicar em {string}') do |botao|
    $comissao.seleciona_janela_confirmacao(botao)
end

Dado('que eu selecione apenas uma parcela do acordo') do
    $comissao.seleciona_uma_parcela
    $comissao.busca_dias_atraso
end

Dado('eu clique no botao de {string}') do |proposta|
    $comissao.gera_proposta(proposta)
end

Então('eu devo validar se o valor da despesa de cobrança não se encontra nulo') do
    $comissao.valida_campo_comissao_nulo
end

Então('devo verificar se o cálculo da comissão está correto') do
    within_window(switch_to_window(windows.last)) do
        var = $comissao.busca_comissao
        valor_comissao = var.gsub('.', '').gsub(',', '.').to_f
        calculo_comissao = $comissao.calcula_valor_comissao
        expect(valor_comissao).to eq(calculo_comissao)
    end   
end

Então('eu devo fazer a validação do valor da despesa de cobrança para comissão de giro') do
    within_window(switch_to_window(windows.last)) do
        var = $comissao.busca_comissao
        valor_comissao = var.gsub('.', '').gsub(',', '.').to_f
        calculo_comissao = $comissao.calcula_valor_comissao
        if (valor_comissao!=0.00)
            expect(valor_comissao).to eq(calculo_comissao)
        end
    end    
end