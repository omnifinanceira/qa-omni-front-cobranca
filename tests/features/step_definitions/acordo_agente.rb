Quando('eu selecionar o agente {string}') do |agente|
    @acordo = AcordoAgentePage.new
    @login = LoginPage.new
    @util = Util.new
    @acordo.seleciona_agente(agente)
end

Quando('clicar no menu {string} > {string} > {string}') do |operacional, cobranca, atendimento|
    @acordo.acessa_tela_atendimento(operacional, cobranca, atendimento)
end

Então('devo preencher o campo {string} agente e clicar no botão {string}') do |contrato, pesquisar|
    contrato = @util.pega_contratos_agente_motos()
    @acordo.pesquisa_contrato(contrato, pesquisar)
end

Então('eu devo preencher o campo {string} agente e clicar no botão {string}') do |contrato, pesquisar|
    contrato = @util.pega_contratos_agente_veiculos_leves()
    @acordo.pesquisa_contrato(contrato, pesquisar)
end

Então('eu devo preencher o campo {string} e devo clicar no botão {string}') do |contrato, pesquisar|
    contrato_teste = @util.pega_contratos_capital_de_giro_mova()
    contrato = contrato_teste.to_s
    @acordo.pesquisa_contrato(contrato, pesquisar)
    $contrato_capital_de_giro = []
    $contrato_capital_de_giro << contrato
end

Então('devo preencher o campo contrato com o {string} e clicar no botão {string}') do |contrato, pesquisar|
    @acordo.pesquisa_contrato(contrato, pesquisar)
end

Então('devo validar se a mensagem {string} será exibida') do |mensagem_campo_nulo|
    within_frame(find(:xpath, "//iframe")) do
        expect(page).to have_content(mensagem_campo_nulo)
    end
end

Então('devo validar se uma nova tela exibida com as mensagens {string}, {string}') do |msg1, msg2|
    within_frame(find(:xpath, "//iframe")) do
        expect(page).to have_content(msg1)
        expect(page).to have_content(msg2)
    end
end

Então('eu devo remover o valor proposto') do
    campo = 'valorTotalBoletoAVista'
    @acordo.remove_valor_proposto(campo)
end

Então('devo verificar se o alerta {string} foi exibido') do |mensagem|
    @acordo.verifica_alerta
end

Então('devo verificar se a seguinte mensagem foi exibida {string}') do |mensagem|
    @acordo.verifica_mensagem(mensagem)
end

Então('devo aplicar o desconto no acordo') do
    @acordo.aplica_desconto
end

Então('eu devo remover o valor do honorário de entrada') do
    campo = 'pvalor_entrada'
    @acordo.remove_valor_honorario(campo)
end

Então('eu devo remover também o valor de honorário para a primeira parcela') do
    campo = 'pvalor_parcela'
    @acordo.remove_valor_honorario(campo)
end

Então('validar o contrato de moto') do
    within_frame(find(:xpath, "//iframe")) do 
        expect(@acordo).to have_contrato_motos
    end
end

Então('validar o contrato de veículo leve') do
    within_frame(find(:xpath, "//iframe")) do 
        expect(@acordo).to have_contrato_veiculos_leves
    end
end

Então('devo ver na tela o acordo gerado anteriormente') do
    contrato1 = $contrato_capital_de_giro.to_s 
    contrato2 = contrato1.scan(/\d+/).map(&:to_i)
    contrato3 = contrato2[0]
    expect(page).to have_content(contrato3)
end

Então('não devo ver na tela o acordo gerado anteriormente') do
    contrato1 = $contrato_capital_de_giro.to_s 
    contrato2 = contrato1.scan(/\d+/).map(&:to_i)
    contrato3 = contrato2[0]
    expect(page).to have_no_content(contrato3)
end

Quando('eu selecionar o botão {string}') do |boleto|
    @acordo.consulta_divida(boleto)
end

Então('devo verificar se uma nova tela foi exibida') do
    expect(page.driver.browser.window_handles.length).to eq(2)
end

Dado('que eu selecione todas as parcelas vencidas do acordo') do 
    @acordo.seleciona_parcelas_vencidas
end

Dado('clique em {string}') do |proposta|
    @acordo.gera_proposta(proposta)
end

Então('eu devo incluir um valor proposto menor do que o existente') do
    @acordo.altera_valor_proposto
end

Então('clicar em {string}') do |mensagem|
    @acordo.seleciona_janela_confirmacao(mensagem)
end

Então('eu devo incluir um valor de honorário menor do que o existente') do
    @acordo.altera_honorario
end

Então('eu devo incluir um valor de honorário menor do que o existente para a entrada') do
    @acordo.altera_entrada_honorario
end

Então('eu clicar em recalcular a proposta') do
    @acordo.recalcula_valores
end

Então('eu devo abaixar também o valor de honorário para a primeira parcela') do
    @acordo.altera_honorario
end

Então('devo selecionar a opção a prazo') do
    @acordo.seleciona_opcao_a_prazo
end

Quando('eu clicar no botão {string} ou no botão {string}') do |boleto, aprovacao|
    @acordo.emite_boleto_ou_solicita_aprovacao(boleto, aprovacao)
end

Então('devo verificar se o acordo foi gerado com sucesso') do
    @acordo.verifica_acordos_gerados
end

Quando('eu clicar no botão {string}') do |boleto|
    @acordo.emite_boleto(boleto)
end

Então('devo ver a tela de confirmação do acordo') do
    within_window(switch_to_window(windows.last)) do 
        expect(@acordo).to have_tela_confirmacao_acordo
    end
end

Quando('eu selecionar novamente o botão {string}') do |boleto|
    within_window(switch_to_window(windows.first)) do 
        @acordo.consulta_divida(boleto)
    end
end

Então('devo verificar se as parcelas selecionadas anteriormente estão informando que o acordo foi feito') do
    within_window(switch_to_window(windows.last)) do 
        expect(@acordo).to have_validacao_acordo
    end
end

Então('devo incluir um comentário') do
    @acordo.inclui_comentario
end

Quando('eu clicar em {string}') do |aprovacao|
    @acordo.solicita_aprovacao(aprovacao)
end

Então('devo ver a tela de proposta em análise') do
    within_window(switch_to_window(windows.last)) do 
        expect(@acordo).to have_tela_solicitacao_acordo
    end
end