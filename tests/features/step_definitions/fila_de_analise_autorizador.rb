Dado('que eu selecione a opção Autorizador do menu') do 
    @fila_autorizador = FilaAnaliseAutorizadorPage.new
    @fila_autorizador.seleciona_menu_autorizador
end

Dado('selecione o agente {string} do contrato') do |agente_contrato|
    @fila_autorizador.seleciona_agente_do_contrato
end

Dado('selecione o agente {string} do acordo') do |agente_acordo|
    @fila_autorizador.seleciona_agente_do_acordo
end

Quando('eu selecionar a fila {string}') do |fila|
    @fila_autorizador.seleciona_fila_banco
    sleep(2)
end