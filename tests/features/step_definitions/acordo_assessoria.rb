Então('devo preencher o campo {string} assessoria e clicar no botão {string}') do |contrato, pesquisar|    
    contrato = @util.pega_contratos_assessoria_motos()
    @acordo.pesquisa_contrato(contrato, pesquisar)
end

Então('eu devo preencher o campo {string} assessoria e clicar no botão {string}') do |contrato, pesquisar|
    contrato = @util.pega_contratos_assessoria_veiculos_leves()
    @acordo.pesquisa_contrato(contrato, pesquisar)
end

Quando('eu selecionar o botão de {string}') do |boleto|
    @acordo_agente = AcordoAgentePage.new
    @acordo_assessoria = AcordoAssessoriaPage.new
    @login = LoginPage.new
    @acordo_assessoria.consulta_divida(boleto)
end

Quando('devo verificar se a janela de confirmação será exibida e clicar em {string}') do |botao|
    @acordo_assessoria.seleciona_janela_confirmacao(botao)
end

Dado('que todas as parcelas vencidas do acordo sejam selecionadas') do 
    @acordo_assessoria.seleciona_parcelas_vencidas
end

Dado('eu clique em {string}') do |proposta|
    @acordo_assessoria.gera_proposta(proposta)
end

Então('devo incluir um valor de honorário menor do que o existente') do
    @acordo_assessoria.altera_honorario
end

Então('clicar em recalcular a proposta') do
    @acordo_assessoria.recalcula_valores
end

Quando('eu clicar no botão de {string} ou no botão {string}') do |boleto, aprovacao|
    @acordo_assessoria.emite_boleto_ou_solicita_aprovacao(boleto, aprovacao)
end

Então('eu devo verificar se o acordo foi gerado com sucesso') do
    @acordo_assessoria.verifica_acordos_gerados
end

Então('eu devo aplicar o desconto no acordo') do
    @acordo_assessoria.aplica_desconto
end

Então('devo remover o valor proposto') do
    campo = 'valorTotalBoletoAVista'
    @acordo_assessoria.remove_valor_proposto(campo)
end

Então('eu devo verificar se o alerta {string} foi exibido') do |mensagem|
    @acordo_assessoria.verifica_alerta
end

Então('devo incluir um valor proposto menor do que o existente') do
    @acordo_assessoria.altera_valor_proposto
end

Então('vou clicar em {string}') do |mensagem|
    @acordo_assessoria.seleciona_alerta_confirmacao(mensagem)
end

Então('eu devo verificar se a seguinte mensagem foi exibida {string}') do |mensagem|
    @acordo_assessoria.verifica_mensagem(mensagem)
end

Então('devo remover o valor do honorário de entrada') do
    campo = 'pvalor_entrada'
    @acordo_assessoria.remove_valor_honorario(campo)
end

Então('devo remover também o valor de honorário para a primeira parcela') do
    campo = 'pvalor_parcela'
    @acordo_assessoria.remove_valor_honorario(campo)
end

Quando('eu clicar no botão de {string}') do |aprovacao|
    @acordo_assessoria.solicita_aprovacao(aprovacao)
end

Então('eu devo ver a tela de proposta em análise') do
    within_window(switch_to_window(windows.last)) do 
        expect(@acordo_agente).to have_tela_solicitacao_acordo
    end
end

Então('eu devo selecionar a opção a prazo') do
    @acordo_assessoria.seleciona_opcao_a_prazo
end

Então('devo incluir um valor de honorário menor do que o existente para a entrada') do
    @acordo_assessoria.altera_entrada_honorario
end

Então('devo abaixar também o valor de honorário para a primeira parcela') do
    @acordo_assessoria.altera_honorario
end

Quando('clicar no botão {string}') do |boleto|
    @acordo_assessoria.emite_boleto(boleto)
end

Quando('selecionar novamente o botão {string}') do |boleto|
    within_window(switch_to_window(windows.first)) do 
        @acordo_assessoria.consulta_divida(boleto)
        @acordo_assessoria.seleciona_janela_confirmacao('OK')
    end
end

Então('eu devo incluir um comentário') do
    @acordo_assessoria.inclui_comentario
end