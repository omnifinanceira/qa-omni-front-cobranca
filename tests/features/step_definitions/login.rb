Dado('que eu estou na página de login do Omni Fácil') do
    @login = LoginPage.new
    @login.load
end

Quando('eu faço login com {string} e {string}') do |usuario, senha|
    @login.load
    @login.faz_login(usuario,senha)
end

Então('devo ver a mensagem {string} no dashboard') do |mensagem|
    @login.valida_login(mensagem)
end

Dado('faço login com {string} e {string}') do |usuario, senha|
    @login.faz_login(usuario,senha)
end

Então('devo verificar se a mensagem {string} sera exibida') do |mensagem|
    @login.valida_login(mensagem)
    expect(@login).to have_box_alerta
end

Quando('clico no meu usuário {string} e depois em {string}') do |usuario, sair|
    @login.faz_logout(usuario,sair)
end

Então('devo verificar se sou redirecionado para a tela de login') do
    expect(@login).to have_login_user
end