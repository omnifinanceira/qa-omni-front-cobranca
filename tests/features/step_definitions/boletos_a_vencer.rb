Quando('eu selecionar o agente {string} na tela do omnifacil') do |agente|
    @boletos = BoletosAVencer.new
    @login = LoginPage.new
    @util = Util.new
    @boletos.seleciona_agente(agente)
  end

  Quando('clicar no menu {string} > {string} > {string} > {string}') do |operacional, cobranca, relatorio, boletos|
    @boletos.acessa_tela_boletos_a_vencer(operacional, cobranca, relatorio,boletos)
  end

  Quando('eu preencho o campo de data inicial {string} e o campo de data final {string}') do |dtInicial, dtFinal|
    @boletos.selecionar_datas(dtInicial,dtFinal)
  end
  
  Quando('eu selecione o filtro de usuario acordo {string} na tela de pesquisa do omnifacil') do |usuario|
    @boletos.selecionar_usuario(usuario)
  end
  
  Quando('eu selecione o filtro de agentes {string} na tela de pesquisa do omnifacil') do |agentes_boletos|
    @boletos.selecionar_agentes(agentes_boletos)
  end
  
  Quando('eu selecione o filtro de produtos {string} na tela de pesquisa do omnifacil') do |produtos|
    @boletos.selecionar_produtos(produtos)
  end
  
  Quando('clique no botao de pesquisar') do
    @boletos.botao_pesquisar
  end

  Então('a tela deve retornar na tela os boletos filtrados com apenas o produto {string}') do |produto|
    @boletos.valida_produto_selecionado_tela_relatorios(produto)
  end

  Então('a tela deve retornar na tela os boletos filtrados com apenas o usuário {string}') do |usuario|
    @boletos.valida_usuario_selecionado_tela_relatorios(usuario)
  end

  Então('a tela deve retornar na tela os boletos filtrados com apenas o produto {string} e o usuário {string}') do |produto, usuario|
    @boletos.valida_produto_selecionado_tela_relatorios(produto)
    @boletos.valida_usuario_selecionado_tela_relatorios(usuario)
  end
  
  Então('a tela não deve retornar nenhum resultado') do
    @boletos.valida_tela_relatorios_sem_retorno
  end