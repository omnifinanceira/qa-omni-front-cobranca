class LoginPage < SitePrism::Page

    set_url '/pck_login.prc_login'

    element :login_user, '#p-nome'
    element :login_password, '#p-senha'
    element :login_button, '#btn-conectar'
    elements :box_alerta, 'div[class="jconfirm-box jconfirm-hilight-shake jconfirm-type-orange jconfirm-type-animated"]'

    def faz_login(usuario,senha)
        login_user.set usuario
        login_password.set senha
        login_button.click
    end

    def valida_login(mensagem)
        page.has_text?(mensagem)
    end

    def busca_link(botao)
        first('a', :text => botao).click
    end

    def busca_botao(botao)
        first('button', :text => botao).click
    end

    def faz_logout(usuario, sair)
        busca_link(usuario)
        busca_link(sair)
        busca_botao('SAIR')
    end

end