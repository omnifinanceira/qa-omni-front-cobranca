class Util < SitePrism::Page

    def initialize
        @sql = Queries.new
        $wait = Selenium::WebDriver::Wait.new(:timeout => 30)
    end

    def pega_contratos_agente_motos()
        lista_contratos_agente_motos = @sql.busca_contratos_agente_motos
        lista_contratos_agente_motos = lista_contratos_agente_motos.to_s
        contratos = lista_contratos_agente_motos.scan(/\d+/).map(&:to_i)
        return contratos.to_s
    end

    def pega_contratos_agente_veiculos_leves()
        lista_contratos_agente_veiculos_leves = @sql.busca_contratos_agente_veiculos_leves
        lista_contratos_agente_veiculos_leves = lista_contratos_agente_veiculos_leves.to_s
        contratos = lista_contratos_agente_veiculos_leves.scan(/\d+/).map(&:to_i)
        return contratos.to_s
    end

    def pega_contratos_assessoria_motos()
        lista_contratos_assessoria_motos = @sql.busca_contratos_assessoria_motos
        lista_contratos_assessoria_motos = lista_contratos_assessoria_motos.to_s
        contratos = lista_contratos_assessoria_motos.scan(/\d+/).map(&:to_i)
        return contratos.to_s
    end

    def pega_contratos_assessoria_veiculos_leves()
        lista_contratos_assessoria_veiculos_leves = @sql.busca_contratos_assessoria_veiculos_leves
        lista_contratos_assessoria_veiculos_leves = lista_contratos_assessoria_veiculos_leves.to_s
        contratos = lista_contratos_assessoria_veiculos_leves.scan(/\d+/).map(&:to_i)
        return contratos.to_s
    end

    def pega_contratos_capital_de_giro_mova()
        lista_contratos_capital_de_giro_mova = @sql.busca_contratos_capital_de_giro_mova
        lista_contratos_capital_de_giro_mova = lista_contratos_capital_de_giro_mova.to_s
        contratos = lista_contratos_capital_de_giro_mova.scan(/\d+/).map(&:to_i)
        return contratos
    end

end