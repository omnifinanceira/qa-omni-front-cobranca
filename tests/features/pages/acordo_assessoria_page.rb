require 'rspec/core' 
require 'rspec/expectations'

class AcordoAssessoriaPage < SitePrism::Page
    include RSpec::Matchers

    def initialize
        @classe_login = LoginPage.new
        @classe_acordo = AcordoAgentePage.new
    end

    def consulta_divida(boleto)
        within_frame(find(:xpath, "//iframe")) do
            @classe_login.busca_botao(boleto)
        end
    end

    def seleciona_janela_confirmacao(botao)
        within_frame(find(:xpath, "//iframe")) do
            @nova_janela = window_opened_by {
            @classe_login.busca_botao(botao)
            }
        end
    end

    def seleciona_parcelas_vencidas
        within_window @nova_janela do
            @classe_acordo.busca_botao_por_valor('VE').click
        end
    end

    def gera_proposta(proposta)
        within_window @nova_janela do
            @classe_acordo.busca_botao_por_valor(proposta).click
        end
    end

    def altera_honorario
        campo = 'pHonorarioAssessoria'
        within_window @nova_janela do
            @classe_acordo.calcula_porcentagem(campo)
        end
    end

    def altera_entrada_honorario
        campo = 'pHonorarioAssessoriaEntrada'
        within_window @nova_janela do
            @classe_acordo.calcula_porcentagem(campo)
        end
    end

    def recalcula_valores
        within_window @nova_janela do
            first(:xpath, "//img[@alt='Recalcular']").click
        end
    end

    def emite_boleto_ou_solicita_aprovacao(boleto, aprovacao)
        within_window @nova_janela do
            if (page.has_xpath?("//input[@value='#{aprovacao}']"))
                find(:xpath, "//textarea[@name='pobservacao_proposta']").set($comentario)
                @classe_acordo.busca_botao_por_valor(aprovacao).click
            else
                @classe_acordo.busca_botao_por_valor(boleto).click
                find(:xpath, "//input[@id='popup_ok']").click
            end
        end
    end

    def valida_tipo_acordo
        within_window @nova_janela do
            if (page.has_no_xpath?("//input[@value='Enviar SMS e E-Mail']"))
                return true
            else
                return false
            end
        end
    end

    def remove_valor_proposto(campo)
        within_window @nova_janela do
            inputText = find(:xpath, "//input[@id='#{campo}']")
            length = inputText.value.length
            keys = Array.new(length, :backspace)
            find(:xpath, "//input[@id='#{campo}']").send_keys keys
        end
    end

    def altera_valor_proposto
        campo = 'valorTotalBoletoAVista'
        within_window @nova_janela do
            @classe_acordo.calcula_porcentagem_a_vista(campo)
        end
    end

    def seleciona_alerta_confirmacao(botao)
        within_window @nova_janela do
            find(:xpath, "//input[@id='popup_ok']").click
        end
    end

    def verifica_alerta
        within_window @nova_janela do
            page.driver.browser.switch_to.alert.dismiss
        end
    end

    def verifica_mensagem(mensagem)
        within_window @nova_janela do
            expect(page).to have_content(mensagem)
        end
    end

    def remove_valor_honorario(campo)
        within_window @nova_janela do
            inputText = find(:xpath, "//input[@name='#{campo}']")
            length = inputText.value.length
            keys = Array.new(length, :backspace)
            find(:xpath, "//input[@name='#{campo}']").send_keys keys
        end
    end

    def verifica_acordos_gerados
        if (valida_tipo_acordo() == false)
            within_window(switch_to_window(windows.first)) do
                consulta_divida('Emissão de Boleto')
                seleciona_janela_confirmacao('OK')
            end
            within_window(switch_to_window(windows.last)) do 
                first(:xpath, "//font[@color='green']")
            end
        else
            within_window @nova_janela do
            find(:xpath, "//td[contains(text(),'A proposta de acordo foi solicitada com sucesso, e')]")
            end
        end
    end

    def aplica_desconto
        within_window @nova_janela do 

            if (page.has_xpath?("//input[@name='pvalor_entrada']"))
                    entrada = 'pvalor_entrada'
                    campo = entrada
                else
                    parcela_unica = 'pproposta'
                    campo = parcela_unica
            end

            @classe_acordo.calcula_porcentagem(campo)
        end
    end

    def seleciona_opcao_a_prazo
        within_window @nova_janela do 
            sleep(4)
            find('option[value="2"]').select_option
            sleep(4)
        end
    end

    def emite_boleto(boleto)
        within_window @nova_janela do
            @classe_acordo.busca_botao_por_valor(boleto).click
            find(:xpath, "//input[@id='popup_ok']").click
        end
    end

    def inclui_comentario
        within_window @nova_janela do 
        find(:xpath, "//textarea[@name='pobservacao_proposta']").set($comentario)
        end
    end

    def solicita_aprovacao(aprovacao)
        within_window @nova_janela do
            sleep(4)
            @classe_acordo.busca_botao_por_valor(aprovacao).click
            sleep(4)
        end
    end

end