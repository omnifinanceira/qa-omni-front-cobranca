require 'rspec/core' 
require 'rspec/expectations'

class ComissaoPage < SitePrism::Page
    include RSpec::Matchers

    elements :contrato_veiculos_leves, :xpath, "//dd[contains(text(),'VEÍCULO LEVE')]"
    elements :contrato_veiculos_pesados, :xpath, "//font[@size='2']/dd[contains(text(),'VEÍCULO PESADO')]"
    elements :contrato_capital_giro, :xpath, "//font[@size='2']/dd[contains(text(),'CAPITAL DE GIRO')]"

    def initialize
        @classe_login = LoginPage.new
        @classe_acordo_agente = AcordoAgentePage.new
    end

    def consulta_divida(boleto)
        within_frame(find(:xpath, "//iframe")) do
            @classe_login.busca_botao(boleto)
        end
    end

    def seleciona_janela_confirmacao(botao)
        within_frame(find(:xpath, "//iframe")) do
            @nova_janela = window_opened_by {
            @classe_login.busca_botao(botao)
            }
        end
    end

    def seleciona_uma_parcela
        within_window @nova_janela do
            first(:xpath, "//input[@name='pacordo']").click
        end
    end

    def busca_dias_atraso
        within_window @nova_janela do
            caminho = first(:xpath, "//input[@name='pacordo']").path
            linha = caminho.gsub('/HTML/BODY[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/', '').gsub('/TD[10]/FONT[1]/INPUT[1]', '').scan(/\d+/).map(&:to_i)
            $dias_atraso = first(:xpath, "//tr#{linha}/td[3]/font[1]").text
            return $dias_atraso
        end
    end

    def valida_contratos_herdados
        within_window(switch_to_window(windows.first)) do
            within_frame(find(:xpath, "//iframe")) do
        
            busca_contrato = first(:xpath, "//font[1]/dd[1]").text
            contrato = busca_contrato[2,5]
            contrato_convertido = contrato.to_i

            first(:xpath, "//a[@title='Consulta os dados do agente responsável']").click
            busca_agente = find(:xpath, "//div[@class='x_title']/h2[1]").text
            agente = busca_agente[0,4]
            remove_string_agente = agente.scan(/\d+/).map(&:to_i)
            agente_convertido = remove_string_agente.join.to_i
            find(:xpath, "//div[2]/font[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]").click
            
            if (contrato_convertido != agente_convertido)
                return true
            else
                return false
            end
            end
        end
    end

    def porcentagem
    herdados = valida_contratos_herdados()

    if(page.has_xpath?("//font[@size='2']/dd[contains(text(),'BU ADQUIRIDAS')]") or herdados == true)
            dias = $dias_atraso.to_i
            case dias
            when 5..10
                porcentagem = ('0,03')
            when 11..30
                porcentagem = ('0,03')
            when 31..180
                porcentagem = ('0,1')
            when 181..720
                porcentagem = ('0,12')
            when 721..1080
                porcentagem = ('0,15')
            when 1081..9999
                porcentagem = ('0,2')
            else
                "Erro porcentagem de comissão."
            end
    else
            dias = $dias_atraso.to_i
            case dias
            when 11..90
                porcentagem = ('0,1')
            when 91..180
                porcentagem = ('0,12')
            when 181..360
                porcentagem = ('0,15')
            when 361..720
                porcentagem = ('0,2')
            when 721..1080
                porcentagem = ('0,25')
            when 1080..9999
                porcentagem = ('0,3')
            else
                "Erro porcentagem de comissão."
            end
        end
    end

    def gera_proposta(proposta)
        within_window @nova_janela do
            @classe_acordo_agente.busca_botao_por_valor(proposta).click
        end
    end

    def busca_comissao
        within_window @nova_janela do
            campo = 'pHonorarioAssessoria'
            inputText = find(:xpath, "//input[@name='#{campo}']")
            valor_comissao = inputText.value
            return valor_comissao
        end
    end

    def busca_valor_proposto
        within_window(switch_to_window(windows.last)) do
            campo = 'valorTotalBoletoAVista'
            inputText = find(:xpath, "//input[@id='#{campo}']")
            valor_proposto = inputText.value
            return valor_proposto
        end
    end

    def busca_total_divida
        total_divida = find(:xpath, "//tr[2]/td[2]/font[1]").text
        return total_divida
    end

    def busca_despesas
        despesas = find(:xpath, "//td[1]/table[1]/tbody[1]/tr[5]/td[2]/font[1]").text
        return despesas
    end

    def valida_campo_comissao_nulo
        parcela = $dias_atraso.to_i
        if(parcela<0 or parcela<11)
            puts "Validação não será necessária."
        else
            comissao = busca_comissao()
            expect(comissao).not_to eq('0,00')
        end
    end    

    def calcula_valor_acordo
        var1 = porcentagem()
        comissao = var1.gsub('.', '').gsub(',', '.').to_f
        var2 = busca_valor_proposto()
        valor_total = var2.gsub('.', '').gsub(',', '.').to_f
        var3 = busca_total_divida()
        valor_corrigido = var3.gsub('.', '').gsub(',', '.').to_f
        var4 = busca_despesas()
        despesas = var4.gsub('.', '').gsub(',', '.').to_f

        calculo1 = (valor_corrigido+despesas)/(valor_corrigido+despesas)
        calculo2 = ((calculo1*comissao)+1)
        valor_acordo = (valor_total/calculo2)

        return valor_acordo
    end

    def calcula_valor_comissao
        valor_acordo = calcula_valor_acordo()
        var = porcentagem()
        comissao = var.gsub('.', '').gsub(',', '.').to_f

        calculo1 = (valor_acordo*comissao)
        valor_comissao = calculo1.truncate(2).to_f

        return valor_comissao
    end

end