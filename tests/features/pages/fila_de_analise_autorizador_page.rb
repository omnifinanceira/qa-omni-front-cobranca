class FilaAnaliseAutorizadorPage < SitePrism::Page

    def seleciona_menu_autorizador
        first(:xpath, "//a[@id='sel-sistema[5]']/span").click
    end

    def seleciona_agente_do_contrato
        find(:xpath, "//select[@name='p_agente_contrato']/option[@value='2731']").select_option
    end

    def seleciona_agente_do_acordo
        find(:xpath, "//select[@name='p_agente_acordo']/option[@value='184']").select_option
    end

    def seleciona_fila_banco
        find(:xpath, "//select[@name='p_local_analise']/option[@value='17']").select_option
    end

end