require 'faker'
require 'selenium-webdriver'
require 'rspec/core' 
require 'rspec/expectations'

class AcordoAgentePage < SitePrism::Page
    include RSpec::Matchers

    element :combobox_agente, '#select2-p-agente-container'
    element :campo_busca_agente, 'input[class="select2-search__field"]'
    element :botao_validar, '#bt-validar'
    elements :tela_confirmacao_acordo, :xpath, "//input[@value='Enviar SMS e E-Mail']"
    elements :validacao_acordo, :xpath, "//font[@color='green']"
    elements :tela_solicitacao_acordo, :xpath, "//td[contains(text(),'A proposta de acordo foi solicitada com sucesso, e')]"
    elements :contrato_motos, :xpath, "//dd[contains(text(),'MOTO')]"
    elements :contrato_veiculos_leves, :xpath, "//dd[contains(text(),'VEÍCULO LEVE')]"

    def initialize
        @classe_login = LoginPage.new
        @util = Util.new
        $comentario = Faker::Lorem.characters(number: 40)
    end

    def busca_botao_por_valor(value)
        find(:xpath, "//input[@value='#{value}']")
    end

    def seleciona_agente(agente)
        first(:xpath, "//a[@id='sel-sistema[1]']/span/font").click
        $wait.until{combobox_agente.click}
        campo_busca_agente.set(agente)
        first(:xpath, "//li[@class='select2-results__option select2-results__option--highlighted']").click
        botao_validar.click
    end

    def acessa_tela_atendimento(operacional, cobranca, atendimento)
        @classe_login.busca_link(operacional)
        @classe_login.busca_link(cobranca)
        @classe_login.busca_link(atendimento)
    end

    def pesquisa_contrato(contrato, pesquisar)
        within_frame(find(:xpath, "//iframe")) do
            find(:xpath, "//input[@name='p_contrato']").set(contrato)
            @classe_login.busca_botao(pesquisar)
        end
    end

    def consulta_divida(boleto)
        within_frame(find(:xpath, "//iframe")) do
            @nova_janela = window_opened_by {
            @classe_login.busca_botao(boleto)
            }
        end
    end

    def seleciona_parcelas_vencidas
        within_window @nova_janela do
            busca_botao_por_valor('VE').click
        end
    end

    def gera_proposta(proposta)
        within_window @nova_janela do
            busca_botao_por_valor(proposta).click
        end
    end

    def seleciona_opcao_a_prazo
        within_window @nova_janela do 
            sleep(4)
            find('option[value="2"]').select_option
            sleep(4)
        end
    end

    def altera_honorario
        campo = 'pHonorarioAssessoria'
        within_window @nova_janela do
            calcula_porcentagem(campo)
        end
    end

    def altera_valor_proposto
        campo = 'valorTotalBoletoAVista'
        within_window @nova_janela do
            calcula_porcentagem_a_vista(campo)
        end
    end

    def altera_entrada_honorario
        campo = 'pHonorarioAssessoriaEntrada'
        within_window @nova_janela do
            calcula_porcentagem(campo)
        end
    end

    def recalcula_valores
        within_window @nova_janela do
            find(:xpath, "//img[@alt='Recalcular']").click
        end
    end

    def emite_boleto_ou_solicita_aprovacao(boleto, aprovacao)
        within_window @nova_janela do
            if (page.has_xpath?("//input[@value='#{aprovacao}']"))
                find(:xpath, "//textarea[@name='pobservacao_proposta']").set($comentario)
                busca_botao_por_valor(aprovacao).click
            else
                busca_botao_por_valor(boleto).click
                find(:xpath, "//input[@id='popup_ok']").click
            end
        end
    end
    
    def valida_tipo_acordo
        within_window @nova_janela do
            if (page.has_no_xpath?("//input[@value='Enviar SMS e E-Mail']"))
                return true
            else
                return false
            end
        end
    end
    
    def verifica_acordos_gerados
        if (valida_tipo_acordo() == false)
            within_window(switch_to_window(windows.first)) do
                consulta_divida('Emissão de Boleto')
            end
            within_window(switch_to_window(windows.last)) do 
                first(:xpath, "//font[@color='green']")
            end
        else
            within_window @nova_janela do
            find(:xpath, "//td[contains(text(),'A proposta de acordo foi solicitada com sucesso, e')]")
            end
        end
    end

    def emite_boleto(boleto)
        within_window @nova_janela do
            busca_botao_por_valor(boleto).click
            find(:xpath, "//input[@id='popup_ok']").click
        end
    end

    def remove_valor_proposto(campo)
        within_window @nova_janela do
            inputText = find(:xpath, "//input[@id='#{campo}']")
            length = inputText.value.length
            keys = Array.new(length, :backspace)
            find(:xpath, "//input[@id='#{campo}']").send_keys keys
        end
    end

    def remove_valor_honorario(campo)
        within_window @nova_janela do
            inputText = find(:xpath, "//input[@name='#{campo}']")
            length = inputText.value.length
            keys = Array.new(length, :backspace)
            find(:xpath, "//input[@name='#{campo}']").send_keys keys
        end
    end

    def verifica_alerta
        within_window @nova_janela do
            page.driver.browser.switch_to.alert.dismiss
        end
    end

    def verifica_mensagem(mensagem)
        within_window @nova_janela do
            expect(page).to have_content(mensagem)
        end
    end

    def calcula_porcentagem(campo)
        inputText = find(:xpath, "//input[@name='#{campo}']")
        valor_inicial = inputText.value
        valor_inicial = valor_inicial.gsub('.', '').gsub(',', '.').to_f
        valor_com_desc = (valor_inicial*0.9).to_f
        length = inputText.value.length
        keys = Array.new(length, :backspace)
        find(:xpath, "//input[@name='#{campo}']").send_keys keys, "%.2f"% "#{valor_com_desc}"
    end

    def calcula_porcentagem_a_vista(campo)
        inputText = find(:xpath, "//input[@id='#{campo}']")
        valor_inicial = inputText.value
        valor_inicial = valor_inicial.gsub('.', '').gsub(',', '.').to_f
        valor_com_desc = (valor_inicial*0.9).to_f
        length = inputText.value.length
        keys = Array.new(length, :backspace)
        find(:xpath, "//input[@id='#{campo}']").send_keys keys, "%.2f"% "#{valor_com_desc}"
    end

    def aplica_desconto
        within_window @nova_janela do 

            if (page.has_xpath?("//font[contains(text(),'Entrada')]"))
                    entrada = 'pvalor_entrada'
                    campo = entrada
                else
                    parcela_unica = 'pproposta'
                    campo = parcela_unica
            end

            calcula_porcentagem(campo)
        end
    end

    def seleciona_janela_confirmacao(botao)
            within_window @nova_janela do
                find(:xpath, "//input[@id='popup_ok']").click
        end
    end

    def inclui_comentario
        within_window @nova_janela do 
            find(:xpath, "//textarea[@name='pobservacao_proposta']").set($comentario)
        end
    end

    def solicita_aprovacao(aprovacao)
        within_window @nova_janela do 
            sleep(4)
            busca_botao_por_valor(aprovacao).click
            sleep(4)
        end
    end
    
end