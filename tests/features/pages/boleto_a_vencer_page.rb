require 'faker'
require 'selenium-webdriver'

class BoletosAVencer < SitePrism::Page
    include RSpec::Matchers

    element :combobox_agente, '#select2-p-agente-container'
    element :combobox_agentes_tela_boletos, '#select2-p_agentes-container'
    element :combobox_produtos, '#select2-p_produto-container'

    element :campo_busca_agente, 'input[class="select2-search__field"]'
    element :botao_validar, '#bt-validar'
    element :data_inicial, :id, 'dataInicio'
    element :data_final, :id, 'dataFim'
    element :selecionar_usuario_acordo, '#select2-p_usr_agente-container'
    element :buscar_pelo_campo_combobox, :xpath, "//input[@class='select2-search__field']"

    element :mensagem_retorno_agente, :xpath, "//span[contains(text(),'É obrigatório selecionar um agente.')]"
    element :produto, :xpath, "//tbody/tr[1]/td[13]"
    element :usuario, :xpath, "//tbody/tr[1]/td[5]" 

    def initialize
        @classe_login = LoginPage.new
        @util = Util.new
    end

    def busca_botao_por_valor(value)
        find(:xpath, "//input[@value='#{value}']")
    end
    
    def seleciona_agente(agente)
        first(:xpath, "//a[@id='sel-sistema[1]']/span/font").click
        $wait.until{combobox_agente.click}
        campo_busca_agente.set(agente)
        first(:xpath, "//li[@class='select2-results__option select2-results__option--highlighted']").click
        sleep(5)
        botao_validar.click
    end

    def acessa_tela_boletos_a_vencer(operacional, cobranca, atendimento,boleto)
        @classe_login.busca_link(operacional)
        @classe_login.busca_link(cobranca)
        @classe_login.busca_link(atendimento)
        @classe_login.busca_link(boleto)
    end

    def selecionar_datas(inicial,final)
        within_frame(find(:xpath, "//iframe")) do
                data_inicial.set(inicial)
                data_inicial.send_keys inicial, :enter
                data_final.set(final)
                data_final.send_keys final, :enter   
        end
    end        
    
    def selecionar_usuario(usuario_acordo)
        within_frame(find(:xpath, "//iframe")) do
                selecionar_usuario_acordo.click
                buscar_pelo_campo_combobox.send_keys usuario_acordo, :enter   
        end
    end

    def selecionar_agentes(agentes_boletos)
        within_frame(find(:xpath, "//iframe")) do
                combobox_agentes_tela_boletos.click
                buscar_pelo_campo_combobox.send_keys agentes_boletos, :enter
        end
    end

    def selecionar_produtos(produtos)
        within_frame(find(:xpath, "//iframe")) do
                combobox_produtos.click
                sleep(2)
                buscar_pelo_campo_combobox.set(produtos)
                buscar_pelo_campo_combobox.send_keys :enter
        end
    end

    def botao_pesquisar
        within_frame(find(:xpath, "//iframe")) do
            first('button', :text => "Pesquisar").click
        end   
    end
    
    def getMessageAgente
        find(:xpath, mensagem_retorno_agente)
    end

    def valida_produto_selecionado_tela_relatorios(texto)
        within_frame(find(:xpath, "//iframe")) do
            expect(produto).to have_content(texto)
        end
    end

    def valida_usuario_selecionado_tela_relatorios(texto)
        within_frame(find(:xpath, "//iframe")) do
            expect(usuario).to have_content(texto)
        end
    end

    def valida_tela_relatorios_sem_retorno
        within_frame(find(:xpath, "//iframe")) do
            page.should_not have_xpath("//tbody/tr[1]/td[13]")
        end
    end
end
