#language: pt

Funcionalidade: Gerar acordos pelo Omni Fácil
- Para que eu possa acessar o sistema sendo um usuário/assessoria
- Para que eu possa acessar a página de cobrança
- E possa gerar acordos à vista, parcelado e com desconto por contrato
- E possa validar os acordos gerados

Contexto: Pré-requisitos para acessar o Omni Fácil
    * que eu estou na página de login do Omni Fácil
    * faço login com "1801DEJANIRA" e "SENHA123"

@busca_contrato_vazio
Cenário: Realizando busca por contrato vazio na cobrança amigável
    Quando eu selecionar o agente '1801 - OLG - JAÚ-SP'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o ' ' e clicar no botão 'Pesquisar'
    E devo validar se a mensagem 'Todos os campos estão nulos, precisa ser preenchido algum para a pesquisa.' será exibida

@busca_contrato_nulo
Cenário: Realizando busca por contrato nulo na cobrança amigável
    Quando eu selecionar o agente '1801 - OLG - JAÚ-SP'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o '00000' e clicar no botão 'Pesquisar'
    E devo validar se uma nova tela exibida com as mensagens 'Não existe este contrato:', 'Verifique se digitou corretamente.'

@busca_contrato_incorreto
Cenário: Realizando busca por contrato incorreto na cobrança amigável
    Quando eu selecionar o agente '1801 - OLG - JAÚ-SP'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o '544848161211' e clicar no botão 'Pesquisar'
    E devo validar se uma nova tela exibida com as mensagens 'Não existe este contrato:', 'Verifique se digitou corretamente.'

@gerando_acordo_veiculos_leves_assessoria_a_vista
Cenário: Gerando acordo no Omni Fácil com proposta à vista
    * eu selecionar o agente '1801 - OLG - JAÚ-SP'
    * clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    * eu devo preencher o campo 'Contrato' assessoria e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então devo incluir um valor de honorário menor do que o existente
    E clicar em recalcular a proposta
    Quando eu clicar no botão de 'Emissão do Boleto' ou no botão 'Solicita Aprovação'
    Então eu devo verificar se o acordo foi gerado com sucesso

@gerando_acordo_veiculos_leves_assessoria_a_vista_valor_nulo
Cenário: Gerando acordo no Omni Fácil com proposta à vista e valor nulo
    * eu selecionar o agente '1801 - OLG - JAÚ-SP'
    * clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    * eu devo preencher o campo 'Contrato' assessoria e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    * devo remover o valor proposto
    E eu devo verificar se o alerta 'O valor da proposta não pode ser zero' foi exibido

@gerando_acordo_veiculos_leves_assessoria_a_vista_com_desconto
Cenário: Gerando acordo no Omni Fácil com proposta à vista com desconto
    * eu selecionar o agente '1801 - OLG - JAÚ-SP'
    * clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    * eu devo preencher o campo 'Contrato' assessoria e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então devo incluir um valor proposto menor do que o existente
    E clicar em recalcular a proposta
    Então eu devo incluir um comentário
    Quando eu clicar no botão de 'Solicita Aprovação'
    Então eu devo ver a tela de proposta em análise

@gerando_acordo_veiculos_leves_assessoria_a_vista_com_desconto_sem_comentario
Cenário: Gerando acordo no Omni Fácil com proposta à vista com desconto e sem comentário
    * eu selecionar o agente '1801 - OLG - JAÚ-SP'
    * clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    * eu devo preencher o campo 'Contrato' assessoria e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então devo incluir um valor proposto menor do que o existente
    E clicar em recalcular a proposta
    Quando eu clicar no botão de 'Solicita Aprovação'
    E vou clicar em 'OK'
    * eu devo verificar se a seguinte mensagem foi exibida 'Informe a justificativa de acordo, no mínimo 40 caracteres!'

@gerando_acordo_veiculos_leves_assessoria_a_prazo
Cenário: Gerando acordo no Omni Fácil com proposta a prazo
    * eu selecionar o agente '1801 - OLG - JAÚ-SP'
    * clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    * eu devo preencher o campo 'Contrato' assessoria e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então eu devo selecionar a opção a prazo
    E devo incluir um valor de honorário menor do que o existente para a entrada
    Então devo abaixar também o valor de honorário para a primeira parcela
    E clicar em recalcular a proposta
    Quando eu clicar no botão de 'Emissão do Boleto' ou no botão 'Solicita Aprovação'
    * eu devo ver a tela de proposta em análise

@gerando_acordo_veiculos_leves_assessoria_a_prazo_valor_nulo
Cenário: Gerando acordo no Omni Fácil com proposta a prazo e valor nulo
    * eu selecionar o agente '1801 - OLG - JAÚ-SP'
    * clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    * eu devo preencher o campo 'Contrato' assessoria e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então eu devo selecionar a opção a prazo
    * devo remover o valor do honorário de entrada
    Então eu devo verificar se o alerta 'O valor da proposta não pode ser zero' foi exibido
    E devo remover também o valor de honorário para a primeira parcela
    Então eu devo verificar se o alerta 'O valor da proposta não pode ser zero' foi exibido

@gerando_acordo_veiculos_leves_assessoria_a_prazo_com_desconto
Cenário: Gerando acordo no Omni Fácil com proposta a prazo com desconto
    * eu selecionar o agente '1801 - OLG - JAÚ-SP'
    * clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    * eu devo preencher o campo 'Contrato' assessoria e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então eu devo selecionar a opção a prazo
    E devo incluir um valor de honorário menor do que o existente para a entrada
    Então devo abaixar também o valor de honorário para a primeira parcela
    E clicar em recalcular a proposta
    Então eu devo aplicar o desconto no acordo
    E eu devo incluir um comentário
    Quando eu clicar no botão de 'Solicita Aprovação'
    Então eu devo ver a tela de proposta em análise

@gerando_acordo_veiculos_leves_assessoria_a_prazo_com_desconto_sem_comentario
Cenário: Gerando acordo no Omni Fácil com proposta a prazo com desconto e sem comentário
    * eu selecionar o agente '1801 - OLG - JAÚ-SP'
    * clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    * eu devo preencher o campo 'Contrato' assessoria e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então eu devo selecionar a opção a prazo
    E devo incluir um valor de honorário menor do que o existente para a entrada
    Então devo abaixar também o valor de honorário para a primeira parcela
    E clicar em recalcular a proposta
    Então eu devo aplicar o desconto no acordo 
    Quando eu clicar no botão de 'Solicita Aprovação'
    E vou clicar em 'OK'
    * eu devo verificar se a seguinte mensagem foi exibida 'Informe a justificativa de acordo, no mínimo 40 caracteres!'