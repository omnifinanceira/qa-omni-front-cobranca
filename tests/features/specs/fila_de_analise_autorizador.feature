#language: pt

Funcionalidade: Gerar acordos e validá-los na fila de análise do banco no autorizador
- Para que eu possa acessar o sistema sendo um usuário/agente
- Para que eu possa acessar a página de cobrança
- E possa gerar acordos à vista e com desconto por contrato
- E possa validar os acordos gerados
- E também possa validar a fila de análise do banco no autorizador

Contexto: Pré-requisitos para acessar o Omni Fácil
    * que eu estou na página de login do Omni Fácil
    * faço login com "MICHELE" e "SENHA123"

Cenário: Realizando busca por contrato vazio na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o ' ' e clicar no botão 'Pesquisar'
    E devo validar se a mensagem 'Todos os campos estão nulos, precisa ser preenchido algum para a pesquisa.' será exibida

@busca_contrato_nulo
Cenário: Realizando busca por contrato nulo na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o '00000' e clicar no botão 'Pesquisar'
    E devo validar se uma nova tela exibida com as mensagens 'Não existe este contrato:', 'Verifique se digitou corretamente.'

@busca_contrato_incorreto
Cenário: Realizando busca por contrato incorreto na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o '544848161211' e clicar no botão 'Pesquisar'
    E devo validar se uma nova tela exibida com as mensagens 'Não existe este contrato:', 'Verifique se digitou corretamente.'

@gerando_acordo_agente_a_vista_com_desconto
Cenário: Gerando acordo no Omni Fácil com proposta à vista com desconto
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' e devo clicar no botão 'Pesquisar'
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    Então devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então devo incluir um valor proposto menor do que o existente
    E clicar em recalcular a proposta
    Então eu devo incluir um comentário
    Quando eu clicar no botão de 'Solicita Aprovação'
    Então eu devo ver a tela de proposta em análise

@gerando_acordo_agente_a_vista_com_desconto_sem_comentario
Cenário: Gerando acordo no Omni Fácil com proposta à vista com desconto e sem comentário
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' e devo clicar no botão 'Pesquisar'
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    Então devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    Então devo incluir um valor proposto menor do que o existente
    E clicar em recalcular a proposta
    Quando eu clicar no botão de 'Solicita Aprovação'
    E vou clicar em 'OK'
    Então eu devo verificar se a seguinte mensagem foi exibida 'Informe a justificativa de acordo, no mínimo 40 caracteres!'

@gerando_acordo_agente_a_vista_valor_nulo
Cenário: Gerando acordo no Omni Fácil com proposta à vista com valor nulo
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' e devo clicar no botão 'Pesquisar'
    Quando eu selecionar o botão de 'Emissão de Boleto'
    E devo verificar se a janela de confirmação será exibida e clicar em 'OK'
    Então devo verificar se uma nova tela foi exibida
    Dado que todas as parcelas vencidas do acordo sejam selecionadas
    E eu clique em 'Proposta'
    * devo remover o valor proposto
    E eu devo verificar se o alerta 'O valor da proposta não pode ser zero' foi exibido

@validando_acordo_na_fila_autorizador
Cenário: Validando acordo com proposta à vista na fila do autorizador
    Dado que eu selecione a opção Autorizador do menu
    E selecione o agente 'MOVA' do contrato
    E selecione o agente 'BATISTELLA' do acordo
    Quando eu selecionar a fila 'BANCO'
    Então devo ver na tela o acordo gerado anteriormente

@validando_acordo_na_fila_autorizador_buscando_agente_acordo_incorreto
Cenário: Validando acordo com proposta à vista na fila do autorizador buscando pelo agente do acordo incorreto
    Dado que eu selecione a opção Autorizador do menu
    E selecione o agente 'MOVA' do contrato
    E selecione o agente 'PECUNIA' do acordo
    Quando eu selecionar a fila 'BANCO'
    Então não devo ver na tela o acordo gerado anteriormente

@validando_acordo_na_fila_autorizador_buscando_agente_contrato_incorreto
Cenário: Validando acordo com proposta à vista na fila do autorizador buscando pelo agente do contrato incorreto
    Dado que eu selecione a opção Autorizador do menu
    E selecione o agente 'BATISTELLA ' do contrato
    E selecione o agente 'BATISTELLA ' do acordo
    Quando eu selecionar a fila 'BANCO'
    Então não devo ver na tela o acordo gerado anteriormente

@validando_acordo_na_fila_autorizador_buscando_fila_incorreta
Cenário: Validando acordo com proposta à vista na fila do autorizador buscando pela fila incorreta
    Dado que eu selecione a opção Autorizador do menu
    E selecione o agente 'BATISTELLA ' do contrato
    E selecione o agente 'BATISTELLA ' do acordo
    Quando eu selecionar a fila 'COBRANÇA'
    Então não devo ver na tela o acordo gerado anteriormente