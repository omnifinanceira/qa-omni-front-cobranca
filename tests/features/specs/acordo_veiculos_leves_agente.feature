#language: pt

Funcionalidade: Gerar acordos pelo Omni Fácil
- Para que eu possa acessar o sistema sendo um usuário/agente
- Para que eu possa acessar a página de cobrança
- E possa gerar acordos à vista, parcelado e com desconto por contrato
- E possa validar os acordos gerados

Contexto: Pré-requisitos para acessar o Omni Fácil
    * que eu estou na página de login do Omni Fácil
    * faço login com "FLAVIO_CAPPELLAZZO" e "SENHA123"

@busca_contrato_vazio
Cenário: Realizando busca por contrato vazio na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o ' ' e clicar no botão 'Pesquisar'
    E devo validar se a mensagem 'Todos os campos estão nulos, precisa ser preenchido algum para a pesquisa.' será exibida

@busca_contrato_nulo
Cenário: Realizando busca por contrato nulo na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o '00000' e clicar no botão 'Pesquisar'
    E devo validar se uma nova tela exibida com as mensagens 'Não existe este contrato:', 'Verifique se digitou corretamente.'

@busca_contrato_incorreto
Cenário: Realizando busca por contrato incorreto na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o '544848161211' e clicar no botão 'Pesquisar'
    E devo validar se uma nova tela exibida com as mensagens 'Não existe este contrato:', 'Verifique se digitou corretamente.'

@gerando_acordo_veiculos_leves_agente_a_vista
Cenário: Gerando acordo no Omni Fácil com proposta à vista
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' agente e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão 'Emissão de Boleto'
    Então devo verificar se uma nova tela foi exibida
    Dado que eu selecione todas as parcelas vencidas do acordo
    E clique em 'Proposta'
    Quando eu clicar no botão 'Emissão do Boleto' ou no botão 'Solicita Aprovação'
    Então devo verificar se o acordo foi gerado com sucesso

@gerando_acordo_veiculos_leves_agente_a_vista_valor_nulo
Cenário: Gerando acordo no Omni Fácil com proposta à vista e valor nulo
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' agente e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão 'Emissão de Boleto'
    Então devo verificar se uma nova tela foi exibida
    Dado que eu selecione todas as parcelas vencidas do acordo
    E clique em 'Proposta'
    Então eu devo remover o valor proposto
    E devo verificar se o alerta 'O valor da proposta não pode ser zero' foi exibido

@gerando_acordo_veiculos_leves_agente_a_vista_com_desconto
Cenário: Gerando acordo no Omni Fácil com proposta à vista com desconto
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' agente e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão 'Emissão de Boleto'
    Então devo verificar se uma nova tela foi exibida
    Dado que eu selecione todas as parcelas vencidas do acordo
    E clique em 'Proposta'
    Então eu devo incluir um valor proposto menor do que o existente
    E eu clicar em recalcular a proposta
    E devo incluir um comentário
    Quando eu clicar em 'Solicita Aprovação'
    Então devo ver a tela de proposta em análise

@gerando_acordo_veiculos_leves_agente_a_vista_com_desconto_sem_comentario
Cenário: Gerando acordo no Omni Fácil com proposta à vista com desconto e sem comentário
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' agente e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão 'Emissão de Boleto'
    Então devo verificar se uma nova tela foi exibida
    Dado que eu selecione todas as parcelas vencidas do acordo
    E clique em 'Proposta'
    Então eu devo incluir um valor proposto menor do que o existente
    E eu clicar em recalcular a proposta
    Quando eu clicar em 'Solicita Aprovação'
    E clicar em 'OK'
    Então devo verificar se a seguinte mensagem foi exibida 'Informe a justificativa de acordo, no mínimo 40 caracteres!'

@gerando_acordo_veiculos_leves_agente_a_prazo
Cenário: Gerando acordo no Omni Fácil com proposta a prazo
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' agente e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão 'Emissão de Boleto'
    Então devo verificar se uma nova tela foi exibida
    Dado que eu selecione todas as parcelas vencidas do acordo
    E clique em 'Proposta'
    Então devo selecionar a opção a prazo
    Quando eu clicar no botão 'Emissão do Boleto'
    Então devo ver a tela de confirmação do acordo
    Quando eu selecionar novamente o botão 'Emissão de Boleto'
    Então devo verificar se as parcelas selecionadas anteriormente estão informando que o acordo foi feito

@gerando_acordo_veiculos_leves_agente_a_prazo_valor_nulo
Cenário: Gerando acordo no Omni Fácil com proposta a prazo e valor nulo
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' agente e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão 'Emissão de Boleto'
    Então devo verificar se uma nova tela foi exibida
    Dado que eu selecione todas as parcelas vencidas do acordo
    E clique em 'Proposta'
    Então devo selecionar a opção a prazo
    E eu devo remover o valor do honorário de entrada
    Então devo verificar se o alerta 'O valor da proposta não pode ser zero' foi exibido
    E eu devo remover também o valor de honorário para a primeira parcela
    Então devo verificar se o alerta 'O valor da proposta não pode ser zero' foi exibido

@gerando_acordo_veiculos_leves_agente_a_prazo_com_desconto
Cenário: Gerando acordo no Omni Fácil com proposta a prazo com desconto
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' agente e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão 'Emissão de Boleto'
    Então devo verificar se uma nova tela foi exibida
    Dado que eu selecione todas as parcelas vencidas do acordo
    E clique em 'Proposta'
    Então devo selecionar a opção a prazo
    * devo aplicar o desconto no acordo
    E devo incluir um comentário
    Quando eu clicar em 'Solicita Aprovação'
    Então devo ver a tela de proposta em análise

@gerando_acordo_veiculos_leves_agente_a_prazo_com_desconto_sem_comentario
Cenário: Gerando acordo no Omni Fácil com proposta a prazo com desconto e sem comentário
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então eu devo preencher o campo 'Contrato' agente e clicar no botão 'Pesquisar'
    E validar o contrato de veículo leve
    Quando eu selecionar o botão 'Emissão de Boleto'
    Então devo verificar se uma nova tela foi exibida
    Dado que eu selecione todas as parcelas vencidas do acordo
    E clique em 'Proposta'
    Então devo selecionar a opção a prazo
    E eu devo incluir um valor de honorário menor do que o existente para a entrada
    Então eu devo abaixar também o valor de honorário para a primeira parcela
    E eu clicar em recalcular a proposta
    E devo aplicar o desconto no acordo
    Quando eu clicar em 'Solicita Aprovação'
    E clicar em 'OK'
    * devo verificar se a seguinte mensagem foi exibida 'Informe a justificativa de acordo, no mínimo 40 caracteres!'