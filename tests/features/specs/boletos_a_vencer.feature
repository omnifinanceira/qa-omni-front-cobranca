#language: pt

Funcionalidade: Boletos a vencer validar filtro 
- Para que eu possa acessar a página de cobrança amigável
- E eu consiga realizar o filtro dos acordos por Agente ou Produto

Contexto: Pré-requisitos para acessar o Omni Fácil
    * que eu estou na página de login do Omni Fácil
    * faço login com "FLAVIO_CAPPELLAZZO" e "SENHA123"

@boletos_filtro_fluxo_principal
Cenário: Validar filtros de selecao de agentes
    Quando eu selecionar o agente '184 - BATISTELLA -' na tela do omnifacil
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Relatórios' > 'Boletos a Vencer'
    E eu preencho o campo de data inicial '12/12/2020' e o campo de data final '12/12/2022'
    E eu selecione o filtro de usuario acordo '184BATISTELLA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de agentes '184 - BATISTELLA & BATISTELLA LTDA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de produtos 'VEÍCULO LEVE' na tela de pesquisa do omnifacil
    E clique no botao de pesquisar 
    Então a tela deve retornar na tela os boletos filtrados com apenas o produto 'VEÍCULO LEVE' e o usuário '184BATISTELLA'

@boletos_a_vencer_usuario_acordo_filtro_todos
Cenário: Validar selecao usuario acordo, filtro Todos
    Quando eu selecionar o agente '184 - BATISTELLA -' na tela do omnifacil
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Relatórios' > 'Boletos a Vencer'
    E eu preencho o campo de data inicial '12/12/2020' e o campo de data final '12/12/2022'
    E eu selecione o filtro de usuario acordo 'Todos' na tela de pesquisa do omnifacil
    E eu selecione o filtro de agentes '184 - BATISTELLA & BATISTELLA LTDA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de produtos 'moto' na tela de pesquisa do omnifacil
    E clique no botao de pesquisar 
    Então a tela deve retornar na tela os boletos filtrados com apenas o produto 'MOTO'

@boletos_a_vencer_filtrar_usuario_acordo_especifico
Cenário: Validar selecao usuario acordo, filtro 184BATISTELLA
    Quando eu selecionar o agente '184 - BATISTELLA -' na tela do omnifacil
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Relatórios' > 'Boletos a Vencer'
    E eu preencho o campo de data inicial '12/12/2020' e o campo de data final '12/12/2022'
    E eu selecione o filtro de usuario acordo '184BATISTELLA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de agentes '184 - BATISTELLA & BATISTELLA LTDA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de produtos 'VEÍCULO LEVE' na tela de pesquisa do omnifacil
    E clique no botao de pesquisar 
    Então a tela deve retornar na tela os boletos filtrados com apenas o usuário '184BATISTELLA'

@boletos_a_vencer_filtrar_por_todos_produtos
Cenário: Validar selecao produtos, utilizando filtro Todos
    Quando eu selecionar o agente '184 - BATISTELLA -' na tela do omnifacil
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Relatórios' > 'Boletos a Vencer'
    E eu preencho o campo de data inicial '12/12/2020' e o campo de data final '12/12/2022'
    E eu selecione o filtro de usuario acordo '184BATISTELLA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de agentes '184 - BATISTELLA & BATISTELLA LTDA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de produtos 'Todos' na tela de pesquisa do omnifacil
    E clique no botao de pesquisar 
    Então a tela deve retornar na tela os boletos filtrados com apenas o usuário '184BATISTELLA'

@boletos_a_vencer_selecionar_produto_especifico
Cenário: Validar selecao produto, filtro CDC LOJA
    Quando eu selecionar o agente '184 - BATISTELLA -' na tela do omnifacil
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Relatórios' > 'Boletos a Vencer'
    E eu preencho o campo de data inicial '12/12/2020' e o campo de data final '12/12/2022'
    E eu selecione o filtro de usuario acordo 'Todos' na tela de pesquisa do omnifacil
    E eu selecione o filtro de agentes '184 - BATISTELLA & BATISTELLA LTDA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de produtos 'CDC LOJA' na tela de pesquisa do omnifacil
    E clique no botao de pesquisar 
    Então a tela deve retornar na tela os boletos filtrados com apenas o produto 'CDC LOJA'

@boletos_a_vencer_filtro_data_fim_maior_que_inicial
Cenário: Validar filtro data
    Quando eu selecionar o agente '184 - BATISTELLA -' na tela do omnifacil
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Relatórios' > 'Boletos a Vencer'
    E eu preencho o campo de data inicial '12/12/2022' e o campo de data final '12/12/2020'
    E eu selecione o filtro de usuario acordo '184BATISTELLA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de agentes '184 - BATISTELLA & BATISTELLA LTDA' na tela de pesquisa do omnifacil
    E eu selecione o filtro de produtos 'VEÍCULO LEVE' na tela de pesquisa do omnifacil
    E clique no botao de pesquisar 
    Então a tela não deve retornar nenhum resultado