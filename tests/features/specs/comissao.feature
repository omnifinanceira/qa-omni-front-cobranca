#language: pt

Funcionalidade: Gerar acordos pelo Omni Fácil
- Para que eu possa acessar o sistema sendo um usuário/agente
- Para que eu possa acessar a página de cobrança
- E possa validar a comissão para contratos de veículos leves, pesados e moto

Contexto: Pré-requisitos para acessar o Omni Fácil
    * que eu estou na página de login do Omni Fácil
    * faço login com "" e ""

@busca_contrato_vazio
Cenário: Realizando busca por contrato vazio na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o ' ' e clicar no botão 'Pesquisar'
    E devo validar se a mensagem 'Todos os campos estão nulos, precisa ser preenchido algum para a pesquisa.' será exibida

@busca_contrato_nulo
Cenário: Realizando busca por contrato nulo na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o '00000' e clicar no botão 'Pesquisar'
    E devo validar se uma nova tela exibida com as mensagens 'Não existe este contrato:', 'Verifique se digitou corretamente.'

@busca_contrato_incorreto
Cenário: Realizando busca por contrato incorreto na cobrança amigável
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato com o '544848161211' e clicar no botão 'Pesquisar'
    E devo validar se uma nova tela exibida com as mensagens 'Não existe este contrato:', 'Verifique se digitou corretamente.'

@validacao_comissao_contrato_veiculos_leves_propria
Cenário: Validando comissão de contratos de veículos leves própria
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato '100331000092019' e clicar no botão 'Pesquisar'
    E validar o contrato de veículos leves 
    Quando o botão 'Emissão de Boleto' for clicado
    E verificar se a janela de confirmação será exibida e vou clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que eu selecione apenas uma parcela do acordo
    E eu clique no botao de 'Proposta'
    Então eu devo validar se o valor da despesa de cobrança não se encontra nulo
    E devo verificar se o cálculo da comissão está correto 

@validacao_comissao_contrato_veiculos_pesados_propria
Cenário: Validando comissão de contratos de veículos pesados própria
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato '101295000013819' e clicar no botão 'Pesquisar'
    E validar o contrato de veículos pesados 
    Quando o botão 'Emissão de Boleto' for clicado
    E verificar se a janela de confirmação será exibida e vou clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que eu selecione apenas uma parcela do acordo
    E eu clique no botao de 'Proposta'
    Então eu devo validar se o valor da despesa de cobrança não se encontra nulo
    E devo verificar se o cálculo da comissão está correto 

@validacao_comissao_contrato_motos_propria
Cenário: Validando comissão de contratos de motos própria
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato '101295000006020' e clicar no botão 'Pesquisar'
    E validar o contrato de moto 
    Quando o botão 'Emissão de Boleto' for clicado
    E verificar se a janela de confirmação será exibida e vou clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que eu selecione apenas uma parcela do acordo
    E eu clique no botao de 'Proposta'
    Então eu devo validar se o valor da despesa de cobrança não se encontra nulo
    E devo verificar se o cálculo da comissão está correto 

@validacao_comissao_contrato_comissao_giro_adquirida
Cenário: Validando comissão de capital de giro adquirida
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato '102155004971607' e clicar no botão 'Pesquisar'
    E validar o contrato de comissao de giro
    Quando o botão 'Emissão de Boleto' for clicado
    E verificar se a janela de confirmação será exibida e vou clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que eu selecione apenas uma parcela do acordo
    E eu clique no botao de 'Proposta'
    Então eu devo fazer a validação do valor da despesa de cobrança para comissão de giro

@validacao_comissao_contrato_veiculos_leves_herdada
Cenário: Validando comissão de contratos de veículos leves herdada
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato '101834000010319' e clicar no botão 'Pesquisar'
    E validar o contrato de veículos leves 
    Quando o botão 'Emissão de Boleto' for clicado
    E verificar se a janela de confirmação será exibida e vou clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que eu selecione apenas uma parcela do acordo
    E eu clique no botao de 'Proposta'
    Então eu devo validar se o valor da despesa de cobrança não se encontra nulo
    E devo verificar se o cálculo da comissão está correto 

@validacao_comissao_contrato_veiculos_pesados_herdada
Cenário: Validando comissão de contratos de veículos pesados herdada
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato '101917000033018' e clicar no botão 'Pesquisar'
    E validar o contrato de veículos pesados 
    Quando o botão 'Emissão de Boleto' for clicado
    E verificar se a janela de confirmação será exibida e vou clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que eu selecione apenas uma parcela do acordo
    E eu clique no botao de 'Proposta'
    Então eu devo validar se o valor da despesa de cobrança não se encontra nulo
    E devo verificar se o cálculo da comissão está correto 

@validacao_comissao_contrato_motos_herdada
Cenário: Validando comissão de contratos de motos herdada
    Quando eu selecionar o agente '184 - BATISTELLA -'
    E clicar no menu 'OPERACIONAL' > 'COBRANÇA AMIGÁVEL' > 'Atendimento'
    Então devo preencher o campo contrato '101834000025918' e clicar no botão 'Pesquisar'
    E validar o contrato de moto 
    Quando o botão 'Emissão de Boleto' for clicado
    E verificar se a janela de confirmação será exibida e vou clicar em 'OK'
    * devo verificar se uma nova tela foi exibida
    Dado que eu selecione apenas uma parcela do acordo
    E eu clique no botao de 'Proposta'
    Então eu devo validar se o valor da despesa de cobrança não se encontra nulo
    E devo verificar se o cálculo da comissão está correto  