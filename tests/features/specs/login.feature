#language: pt

Funcionalidade: Acessar o sistema Omni Fácil
- Para que eu possa acessar o sistema sendo um usuário
- Para que eu possa validar os campos de login com dados incongruentes
- E possa realizar logout da minha conta

Contexto: Logado
Dado que eu estou na página de login do Omni Fácil

@login_acordo_sucesso
Cenário: Logar no Omni Fácil
    Quando eu faço login com "FLAVIO_CAPPELLAZZO" e "SENHA123"
    Então devo ver a mensagem "FLAVIO_CAPPELLAZZO" no dashboard

@validacao_login_acordo
Cenário: Validacao de campos com dados incongruentes
    Quando faço login com "<usuario>" e "<senha>"
    Então devo verificar se a mensagem "<mensagem>" sera exibida

Exemplos:

    | usuario | senha |        mensagem                   |
    | AMANDA  |       | Favor preencher a senha           |
    |         | 123456| Favor preencher o nome do usuário |
    |         |       | Favor preencher o nome do usuário |
    |QIDNJDNJD| SENHA | Usuário e/ou senha inválido       |

@logout_acordo
Cenário: Realizando logout
Quando eu faço login com "FLAVIO_CAPPELLAZZO" e "SENHA123"
E clico no meu usuário "FLAVIO_CAPPELLAZZO" e depois em "Sair"
Então devo verificar se sou redirecionado para a tela de login