class Queries < SQLConnect

        def busca_contratos_agente_motos()
            lista_contratos_motos = select("with contr_moto AS (
            SELECT con.contrato , 
            ope.grupo2 FROM contratos con
            JOIN operacoes ope ON ope.codigo = con.operacao AND ope.grupo2 IN ('MOTO')
            WHERE 1=1
            AND con.emissao <= to_Date('01012021', 'ddmmyyyy')
            AND con.emissao >= to_Date('01012020', 'ddmmyyyy')
            AND con.agente = 184
            AND NOT EXISTS (SELECT 1
            FROM acordo aco
            WHERE aco.contrato = con.contrato
            AND aco.status IN (0,1,3,4,5,6,7,11,12) )
            AND EXISTS (SELECT 1
            FROM prestacoes pre
            WHERE pre.contrato = con.contrato
            AND pre.data_ven < SYSDATE
            AND pre.liquidacao IS NULL)
            AND ROWNUM < 2)
            SELECT contrato , 
            grupo2 FROM contr_moto")
        return lista_contratos_motos
    end

    def busca_contratos_agente_veiculos_leves()
        lista_contratos_agente_veiculos_leves = select("with contr_leve AS (
            SELECT con.contrato ,
            ope.grupo2 FROM contratos con
            JOIN operacoes ope ON ope.codigo = con.operacao AND ope.grupo2 IN ('VEÍCULO LEVE')
            WHERE 1=1
            AND con.emissao <= to_Date('01012021', 'ddmmyyyy')
            AND con.emissao >= to_Date('01012020', 'ddmmyyyy')
            AND con.agente = 184
            AND NOT EXISTS (SELECT 1
            FROM acordo aco
            WHERE aco.contrato = con.contrato
            AND aco.status IN (0,1,3,4,5,6,7,11,12) )
            AND EXISTS (SELECT 1
            FROM prestacoes pre
            WHERE pre.contrato = con.contrato
            AND pre.data_ven < SYSDATE
            AND pre.liquidacao IS NULL)
            AND ROWNUM < 2)
            SELECT contrato ,
            grupo2 FROM contr_leve")
        return lista_contratos_agente_veiculos_leves
    end

    def busca_contratos_assessoria_motos()
        lista_contratos_assessoria_motos = select("WITH contr_assessoria AS (SELECT co2.contrato
            from ca_arquivo_assessoria ar
            , ca_arquivo_assessoria_contrato co
            , contratos co2
            where co2.contrato = co.contrato
            AND ar.sequencia = co.sequencia_arquivo
            and ar.tipo_arquivo = co.tipo_arquivo
            and ar.tipo_arquivo = 1
            AND ar.assessoria = 1801
            and co.data_retirada is null) ,
            contr_moto AS (SELECT con.contrato
            , ope.grupo2
            FROM contratos con
            JOIN operacoes ope ON ope.codigo = con.operacao AND ope.grupo2 IN ('MOTO')
            JOIN listao lis ON lis.contrato = con.contrato AND lis.valor_principal_divida > 0
            JOIN contr_assessoria bas ON bas.contrato = con.contrato
            WHERE 1=1
            AND NOT EXISTS (SELECT 'S' existe
            FROM PROCESSOS1 pro,
            OCORRENCIAS_PROCESSO oco,
            MOVIMENTO_PROCESSOS mov
            WHERE pro.CONTRATO = con.contrato
            AND pro.COD_TIPO_ACAO IN (3,7)
            AND mov.CONTRATO = pro.CONTRATO
            AND mov.NR_SEQ_PROC = pro.NR_SEQ_PROC
            AND mov.COD_TIPO_ACAO = pro.COD_TIPO_ACAO
            AND oco.CODIGO = mov.COD_OCORRENCIA
            AND oco.COD_TIPO_ACAO = mov.COD_TIPO_ACAO
            AND EXISTS (SELECT 'x'
            FROM MOVIMENTO_PROCESSOS
            WHERE CONTRATO = pro.CONTRATO
            AND COD_TIPO_ACAO = pro.COD_TIPO_ACAO
            AND NR_SEQ_PROC = pro.NR_SEQ_PROC
            AND COD_OCORRENCIA IN (1,2)
            )
            AND NOT EXISTS ( SELECT 'x'
            FROM MOVIMENTO_PROCESSOS
            WHERE CONTRATO = pro.CONTRATO
            AND COD_TIPO_ACAO = pro.COD_TIPO_ACAO
            AND NR_SEQ_PROC = pro.NR_SEQ_PROC
            AND COD_OCORRENCIA = 173)
            UNION
            SELECT 'S' existe
            FROM sj_pasta pas
            ,sj_pasta_contrato pas_con
            WHERE pas.cod_tipo = 1 -- Contrária
            AND pas.tip_status_acao = 'A'
            AND pas_con.cod_pasta = pas.cod_pasta
            AND pas_con.contrato = con.contrato)
            AND NOT EXISTS (SELECT 1
            FROM acordo aco
            WHERE aco.contrato = con.contrato
            AND aco.status IN (0,1,3,4,5,6,7,11,12) )
            AND EXISTS (SELECT 1
            FROM prestacoes pre
            WHERE pre.contrato = con.contrato
            AND pre.data_ven < SYSDATE
            AND pre.liquidacao IS NULL)
            AND ROWNUM < 2)
            SELECT contrato ,
            grupo2 FROM contr_moto")
    return lista_contratos_assessoria_motos
end

def busca_contratos_assessoria_veiculos_leves()
    lista_contratos_assessoria_veiculos_leves = select("WITH contr_assessoria AS (SELECT co2.contrato
        from ca_arquivo_assessoria ar
        , ca_arquivo_assessoria_contrato co
        , contratos co2
        where co2.contrato = co.contrato
        AND ar.sequencia = co.sequencia_arquivo
        and ar.tipo_arquivo = co.tipo_arquivo
        and ar.tipo_arquivo = 1
        AND ar.assessoria = 1801
        and co.data_retirada is null) ,
        contr_leve AS (SELECT con.contrato
        , ope.grupo2
        FROM contratos con
        JOIN operacoes ope ON ope.codigo = con.operacao AND ope.grupo2 IN ('VEÍCULO LEVE')
        JOIN listao lis ON lis.contrato = con.contrato AND lis.valor_principal_divida > 0
        JOIN contr_assessoria bas ON bas.contrato = con.contrato
        WHERE 1=1
        AND NOT EXISTS (SELECT 'S' existe
        FROM PROCESSOS1 pro,
        OCORRENCIAS_PROCESSO oco,
        MOVIMENTO_PROCESSOS mov
        WHERE pro.CONTRATO = con.contrato
        AND pro.COD_TIPO_ACAO IN (3,7)
        AND mov.CONTRATO = pro.CONTRATO
        AND mov.NR_SEQ_PROC = pro.NR_SEQ_PROC
        AND mov.COD_TIPO_ACAO = pro.COD_TIPO_ACAO
        AND oco.CODIGO = mov.COD_OCORRENCIA
        AND oco.COD_TIPO_ACAO = mov.COD_TIPO_ACAO
        AND EXISTS (SELECT 'x'
        FROM MOVIMENTO_PROCESSOS
        WHERE CONTRATO = pro.CONTRATO
        AND COD_TIPO_ACAO = pro.COD_TIPO_ACAO
        AND NR_SEQ_PROC = pro.NR_SEQ_PROC
        AND COD_OCORRENCIA IN (1,2)
        )
        AND NOT EXISTS ( SELECT 'x'
        FROM MOVIMENTO_PROCESSOS
        WHERE CONTRATO = pro.CONTRATO
        AND COD_TIPO_ACAO = pro.COD_TIPO_ACAO
        AND NR_SEQ_PROC = pro.NR_SEQ_PROC
        AND COD_OCORRENCIA = 173)
        UNION
        SELECT 'S' existe
        FROM sj_pasta pas
        ,sj_pasta_contrato pas_con
        WHERE pas.cod_tipo = 1 -- Contrária
        AND pas.tip_status_acao = 'A'
        AND pas_con.cod_pasta = pas.cod_pasta
        AND pas_con.contrato = con.contrato)
        AND NOT EXISTS (SELECT 1
        FROM acordo aco
        WHERE aco.contrato = con.contrato
        AND aco.status IN (0,1,3,4,5,6,7,11,12) )
        AND EXISTS (SELECT 1
        FROM prestacoes pre
        WHERE pre.contrato = con.contrato
        AND pre.data_ven < SYSDATE
        AND pre.liquidacao IS NULL)
        AND ROWNUM < 2)
        SELECT contrato ,
        grupo2 FROM contr_leve")
    return lista_contratos_assessoria_veiculos_leves
end

    def busca_contratos_capital_de_giro_mova()
        lista_contratos_capital_de_giro_mova = select("SELECT con.CONTRATO FROM contratos con 
            WHERE operacao = 8557 
            AND agente = 2731 
            AND ROWNUM < 2 
            AND NOT EXISTS (SELECT 1
            FROM acordo aco
            WHERE aco.contrato = con.contrato
            AND aco.status IN (0,1,3,4,5,6,7,11,12) )
            AND EXISTS (SELECT 1
            FROM prestacoes pre
            WHERE pre.contrato = con.contrato
            AND pre.data_ven < SYSDATE
            AND pre.liquidacao IS NULL)")
        return lista_contratos_capital_de_giro_mova
    end

end