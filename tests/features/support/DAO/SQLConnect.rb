class SQLConnect
    def select(query)
        connectionstring = OCI8.new(BANCO["username"], BANCO["password"], BANCO["dbname"])
        cursor = connectionstring.exec(query)

        result = Array.new
        cursor.fetch() {|row| result << row}

        cursor.close
        connectionstring.logoff

        return result
    end
end