require 'allure-cucumber'
require 'capybara'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require_relative 'helper.rb';
require_relative 'hooks.rb';
require 'oci8'
require 'pry'
require 'selenium-webdriver'
require 'site_prism'
require_relative 'DAO/SQLConnect.rb'

BROWSER = ENV['BROWSER']
AMBIENTE = ENV['AMBIENTE']
CONFIG = YAML.load_file(File.dirname(__FILE__) + "/ambientes/#{AMBIENTE}.yml")
BANCO = YAML.load_file(File.dirname(__FILE__) + "/DAO/config.yml")

World(Helper)
World(Capybara::DSL)
World(Capybara::RSpecMatchers)

Capybara.register_driver :selenium do |app|

    caps = Selenium::WebDriver::Remote::Capabilities.chrome('goog:chromeOptions' => { args: ['start-maximized']})
    Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: caps)

end

    Capybara.configure do |config|
        config.default_driver = :selenium
        config.app_host = CONFIG['url_padrao']
        config.default_max_wait_time = 5
    end

    

    AllureCucumber.configure do |config|
        config.results_directory = "/reports"
        config.clean_results_directory = true
        config.logging_level = Logger::INFO
        config.logger = Logger.new($stdout, Logger::DEBUG)
        config.environment = "staging"
        config.environment_properties = {
        custom_attribute: "Omni Fácil"
    }

        config.categories = File.new("categories/my_custom_categories.json")
    end

    @diretorio = "#{Dir.pwd}/reports"
    Dir.mkdir(@diretorio) unless File.exists?(@diretorio)

    @diretorio = "#{Dir.pwd}/results"
    Dir.mkdir(@diretorio) unless File.exists?(@diretorio)