require 'allure-cucumber'

module Helper

   def tirar_foto(nome_arquivo, resultado)
      caminho_arquivo = "results/screenshots/teste_#{resultado}"
      foto = "#{caminho_arquivo}/#{nome_arquivo}.png"
      attach(page.save_screenshot(foto), 'image/png')

      Allure.add_attachment(
         name: "Attachment",
         source: File.open("#{foto}"),
         type: Allure::ContentType::PNG,
         test_case: true
      ) 
   end

end